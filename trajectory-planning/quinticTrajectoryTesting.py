from quinticTrajectory import *
import math
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('GTK3Cairo')


gCode = quinticTrajectory(psx0=0, psy0=0, psx1=0.1, psy1=0, # prev line pos
                          nsx0=-0.5, nsy0=0.5, nsx1=-0.5, nsy1=0.6, # next line pos
                          pvNorm=200, nvNorm=200, cpAvgV=500, # speeds
                          mls=63, minT=0.00001, maxPos=100, maxSpeed=400) # limits


plt.plot(gCode[2])
plt.show()


