from cubicTrajectory import *
import math
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('GTK3Cairo')

gCode = cubicTrajectory(psx1=0.1, psy1=0, # prev line pos
                          nsx0=-0.5, nsy0=0.5, nsx1=-0.5, nsy1=0.6, # next line pos
                          nvNorm=200, # speed
                          mls=63, minT=0.00001, maxPos=100, maxSpeed=500) # limits

plt.scatter(gCode[0],gCode[1])
plt.show()



